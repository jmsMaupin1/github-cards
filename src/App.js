import React, { Component } from 'react';
import {Card, Button, Search} from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css'
import './App.css';

const BASE_URL = `https://api.github.com`;
const userURL  = user => `${BASE_URL}/users/${user}`;
const searchUsersURL = query => `${BASE_URL}/search/users?q=${query}`

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: {},
      active: false,
      loading: false,
    };

    this.handleToggle = this.handleToggle.bind(this);
    this.resultSelect = this.resultSelect.bind(this);
    this.onSearchChange = this.debouce(this.searchUpdate, 500).bind(this)
  }

  searchUpdate(e, {value}) {
    if(value === "") return;
    this.setState({loading: true})
    fetch(searchUsersURL(value))
      .then(res => res.json())
      .then(json => {
        this.setState({
          results: json.items.map(account => {
            return {
              title: account.login,
              image: account.avatar_url
            }
          }),
          loading: false
        })
      })
  }

  resultSelect(e, {result}) {
    console.log(userURL(result.title))
    fetch(userURL(result.title))
      .then(res => res.json())
      .then(json => this.setState({
        user: json,
        active: true
      }))
  }

  debouce(func, wait, immediate) {
    let timeout;
    return function() {
      let context = this, args = arguments;
      let later = function() {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };
      let callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    }
  }

  handleToggle(e) {
    this.setState({active: !this.state.active})
  }

  render() {
    let {user, active, loading, results, value} = this.state;
    let {avatar_url, name, location, bio} = user;
    return (
      <div className="">
        <Search
          loading={loading}
          onResultSelect={this.resultSelect}
          onSearchChange={this.onSearchChange}
          results={results}
          value={value}
        />
        <Button primary loading={loading} onClick={this.handleToggle}>Get Github Info</Button>
        <br/>
        { active && 
          <Card 
            image={avatar_url}
            header={name}
            meta={location}
            description={bio}
          />
        }
      </div>
    )
  }
}

